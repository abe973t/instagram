//
//  Model.swift
//  Instagram
//
//  Created by Abraham Tesfamariam on 7/19/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage

struct AccountInfo {
    var userData: User?
    var userName, fullName, address, bio: String?
    var imgURL: URL?
    var photo: UIImage?
}

struct Post {
    var uID, userName, profileImgURL, caption, postImgURL, postID: String?
    var likeCount: Int?
    var isLikedByUser: Bool = false
}

// match code with datastucture in place

struct Model {
    // references to the Database and Storage
    var ref = Database.database().reference()
    let storageRef = Storage.storage().reference()
    
    // updates account and adds an imgURL to the account in the Database
    func updateAccount(forUser accountInfo: AccountInfo) {
        
        let imgRef = storageRef.child("images").child((accountInfo.userData?.uid)!).child("\(accountInfo.userName!).png")
        let followersRef = ref.child("usersPublicInfo/\((accountInfo.userData?.uid)!)/followers").ref
        let data = UIImageJPEGRepresentation(accountInfo.photo!, 0.3)
        
        var imgURL: String?
        
        imgRef.putData(data!, metadata: nil) { (metaData, error) in
            if error == nil {
                imgURL = String(describing: metaData?.downloadURL()!)
                
                // add public info
                self.ref.child("usersPublicInfo").child((accountInfo.userData?.uid)!).setValue(["userName": accountInfo.userName!, "bio":accountInfo.bio!, "imgURL": imgURL!])
                followersRef.updateChildValues([(accountInfo.userData?.uid)!:true])
                
                // add sensitive info
                self.ref.child("users").child((accountInfo.userData?.uid)!).setValue(["fullName": accountInfo.fullName!, "address": accountInfo.address!])
            }
        }
 
        //print("User ID: \((accountInfo.userData?.uid)!)")
    }
    
    // gets a list of all the users from Firebase
    func getUsers(completion: @escaping ([[String:Any]]) -> ()) {
        ref.child("usersPublicInfo").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            
            let values = snapshot.value as? [String:Any]
            var users: [[String:Any]] = []
            //var followers: [String] = []
            
            for (key, value) in values! {
                //print(key, value)
                if let data = value as? [String:Any] {
                                    /*let fA = data["followers"] as? [String:Bool]
                    for (follower, isBeingFollowed) in fA! {
                        if isBeingFollowed {
                            followers.append(follower)
                        }
                    }*/
                    users.append(["UID":key, "userName": data["userName"]!, "imgURL": data["imgURL"]!])
                }
            }
            
            completion(users)
            
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    // creates a new post
    func postPhoto(UID: String, photo: UIImage, desc: String) {
        
        let postRef = ref.child("posts").childByAutoId()
        let userRef = ref.child("usersPublicInfo/\(UID)/posts")

        let imgRef = storageRef.child("images").child(UID).child("\(postRef.key).png")
        let data = UIImageJPEGRepresentation(photo, 0.3)
        
        // updates post folder and user folder
        imgRef.putData(data!, metadata: nil) { (metadata, error) in
            if error == nil {
                if let url = metadata?.downloadURL() {
                    let stringURL = String(describing: url)
                    postRef.setValue(["UID": UID, "description": desc, "imgURL": stringURL, "likeCount": 0])
                    userRef.updateChildValues([postRef.key:true])
                }
            }
        }
    }
    
    // updates followers for current user
    func updateFollowers(user:String, follower: String) {
        var changed: Bool = false
        ref.child("usersPublicInfo/\(user)/followers").observeSingleEvent(of: .childAdded, with: { (data) in
            if let followers = data.value as? [String:Bool] {
                for(key, value) in followers {
                    if key == follower && value == true {
                        changed = true
                        self.ref.child("usersPublicInfo/\(user)/followers").updateChildValues([follower:false])
                    } else if key == follower && value == false {
                        changed = true
                        self.ref.child("usersPublicInfo/\(user)/followers").updateChildValues([follower:true])
                    }
                }
                if !changed {
                    self.ref.child("usersPublicInfo/\(user)/followers").updateChildValues([follower:true])
                }
            }
        })
    }
    
    func getFollowersUID(UID: String, completion: @escaping (Any) -> ()) {
        var followerUIDs: [String] = []
        
        ref.child("usersPublicInfo/\(UID)/followers").observeSingleEvent(of: .value, with: { (data) in
            let userFollowersData = data.value as? [String:Any]
            
            if userFollowersData != nil {
                for (followerUID, _) in userFollowersData! {
                    followerUIDs.append(followerUID)
                }
            }
            completion(followerUIDs)
        })
    }
    
    // returns follower's userName and all their posts info
    func getFeedData(followersUIDs: [String], completion: @escaping (Any) -> ()) {
        // iterate thru followerUIDs
        // get usernames and all posts from that follower
        
        var userName: String?
        var profileImgURL: String?
        var posts: [Post] = []
        
            for follower in followersUIDs {
                ref.child("usersPublicInfo/\(follower)").observeSingleEvent(of: .value, with: { (data) in
                    
                    let followersData = data.value as? [String:Any]
                    let postsInfo = followersData?["posts"] as? [String:Bool]
                    let postsLikedData = followersData?["postsLiked"] as? [String:Bool]
                    var postsLikedArr: [String] = []
                    
                    if postsLikedData != nil {
                        for (key, _) in postsLikedData! {
                            postsLikedArr.append(key)
                        }
                    }
                    
                    if postsInfo != nil {
                        var counter = 0
                        
                        for (key, _) in postsInfo! {
                            
                            self.ref.child("posts/\(key)").observeSingleEvent(of: .value, with: { (data) in
                                
                                profileImgURL = followersData?["imgURL"] as? String
                                userName = (followersData?["userName"] as? String)!
                                let postsData = data.value as? [String:Any]
                                if postsData != nil {
                                    let postObject = Post(uID: follower, userName: userName, profileImgURL: profileImgURL, caption: postsData?["description"] as! String, postImgURL: postsData?["imgURL"] as? String, postID: key, likeCount: postsData?["likeCount"] as? Int, isLikedByUser: postsLikedArr.contains(key) ? true : false)
                                    posts.append(postObject)
                                    
                                }
                                
                                if postsInfo?.count == counter {
                                    completion(posts)
                                }
                            })
                            counter = counter + 1
                        }
                    }
                    
                })
        }
    }
    
    // reads through list of posts already liked, if post passed in is on there then it decreases the post's likeCount and removes post from user's postsLiked, otherwise vice versa
    func updateLikeStatus(uID: String, post: Post) {
        var postWasLiked: Bool = false
        var numOfLikes: Int?
        ref.child("usersPublicInfo/\(uID)/postsLiked").observeSingleEvent(of: .value, with: { (data) in
            if let postData = data.value as? [String:Bool] {
                for (key, value) in postData {
                    if key == post.postID && value == true {
                        postWasLiked = true
                    }
                }
        
                // when debugging, likeCount is already at 1.. make sure all likeCounts in db and in postsArr are set to 0 and test again
                if postWasLiked {
                    numOfLikes = post.likeCount! - 1
                    self.ref.child("posts/\(post.postID!)").updateChildValues(["likeCount":numOfLikes!])
                    self.ref.child("usersPublicInfo/\(uID)/postsLiked").updateChildValues([post.postID!:false])

                } else {
                    numOfLikes = post.likeCount! + 1
                    self.ref.child("posts/\(post.postID!)").updateChildValues(["likeCount":numOfLikes!])
                    self.ref.child("usersPublicInfo/\(uID)/postsLiked").updateChildValues([post.postID!:true])
                }
            }
        })
    }
}
