//
//  AccountInfoVC.swift
//  Instagram
//
//  Created by Abraham Tesfamariam on 7/19/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FBSDKLoginKit
import Crashlytics
import SCLAlertView

// NEED TO POPULATE FIELDS W DATA FROM FIREBASE WHEN LOADED

class AccountInfoVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, FBSDKLoginButtonDelegate {

    var imgPicker = UIImagePickerController()
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var bioTxtField: UITextField!
    @IBOutlet weak var siteTxtField: UITextField!
    @IBOutlet weak var userNameTxtField: UITextField!
    @IBOutlet weak var fullNameTxtField: UITextField!
    
    var account: AccountInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // make imgView circular
        imgView.layer.cornerRadius = imgView.frame.width / 2
        imgView.clipsToBounds = true
        // add border
        imgView.layer.borderWidth = 1.5
        imgView.layer.borderColor = UIColor.orange.cgColor
        
        // add fb button
        let fbLoginBtn = FBSDKLoginButton()
        fbLoginBtn.delegate = self
        fbLoginBtn.frame = CGRect(x: 50, y: 600, width: view.frame.width - 100, height: 50)
        if (FBSDKAccessToken.current() != nil) {
            self.view.addSubview(fbLoginBtn)
        }
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error?) {
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("Hey you logged out.")
    }
    
    @IBAction func saveBtnPressed() {
        // call model function
        // alert that info was successfully saved
        // pop view
        
        if imgView.image == nil || bioTxtField.text == nil || siteTxtField.text == nil || userNameTxtField.text == nil || fullNameTxtField.text == nil {
            SCLAlertView().showError("Error!", subTitle: "Please fill in requied fields.")
        } else {
            account = AccountInfo(userData: Auth.auth().currentUser, userName: userNameTxtField.text!, fullName: fullNameTxtField.text!, address: siteTxtField.text!, bio: bioTxtField.text!, imgURL: URL(string: ""), photo: imgView.image)
            
            Model().updateAccount(forUser: account!)

            SCLAlertView().showSuccess("Success!", subTitle: "Your account was updated.", closeButtonTitle: nil, duration: 3, colorStyle: 0xff9100, colorTextButton: 0x3e2723, circleIconImage: nil, animationStyle: .bottomToTop)
            
            if let dVC = storyboard?.instantiateViewController(withIdentifier: "HomeVC") as? HomeVC {
                //present(dVC, animated: true, completion: nil)
                self.navigationController?.pushViewController(dVC, animated: true)
            }
        }
        
    }
    
    @IBAction func signOutBtnTapped(_ sender: UIButton) {
        do {
            try Auth.auth().signOut()
            print("you signed out of firebase")
            if let dVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC {
                present(dVC, animated: true, completion: nil)
            }
            
        } catch {
            print(error.localizedDescription)
        }
    }
    
    
    @IBAction func openGalleryAction(_ sender: UIButton) {
        imgPicker.sourceType = .photoLibrary
        imgPicker.allowsEditing = true
        imgPicker.delegate = self
        present(imgPicker, animated: true, completion: nil)
    }
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        imgView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
