//
//  HomeVC.swift
//  Instagram
//
//  Created by Abraham Tesfamariam on 7/19/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

import UIKit
import FirebaseAuth
import SDWebImage

class HomeVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblView: UITableView!
    
    var followerUIDs: [String] = []
    var posts: [Post] = []
    let uID = Auth.auth().currentUser?.uid
    
    @IBOutlet weak var postBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // set logo for the navbar title
        let imageView = UIImageView(image:#imageLiteral(resourceName: "Instagram_logo"))
        //imageView.frame = CGRect(x: 120, y: 7, width: 20, height: 30)
        self.navigationItem.titleView = imageView
        self.navigationController?.navigationBar.tintColor = UIColor.orange
        
        // get follower's UID's
        // methods nested in each other to better guess when the async calls will complete 
        Model().getFollowersUID(UID: uID!) { (uIDs) in
            if let uid = uIDs as? [String] {
                self.followerUIDs = uid
                
                // get follower's posts
                Model().getFeedData(followersUIDs: uid, completion: { (data) in
                    if let feedData = data as? [Post] {
                        self.posts = feedData
                        print("🔴", self.posts)
                    }
                    
                    DispatchQueue.main.async {
                        self.tblView.reloadData()
                    }
                })
            }
            
        }
        
        postBtn.layer.borderWidth = 2
        postBtn.layer.borderColor = UIColor.orange.cgColor
        
        tblView.backgroundColor = UIColor.white

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Model().getFeedData(followersUIDs: followerUIDs, completion: { (data) in
            if let feedData = data as? [Post] {
                self.posts = feedData
                print("🔴", self.posts)
            }
            
            DispatchQueue.main.async {
                self.tblView.reloadData()
            }
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostViewCell") as! PostViewCell
        let post = posts[indexPath.row]
        
        cell.userNameLbl.text = post.userName
        cell.captionTxtView.text = post.caption
        cell.postImgView.sd_setImage(with: URL(string: post.postImgURL!))
        cell.profilePicImgView.sd_setImage(with: URL(string: post.profileImgURL!))
        
        (post.isLikedByUser) ? cell.likeButton.setImage(#imageLiteral(resourceName: "liked"), for: .normal) : cell.likeButton.setImage(#imageLiteral(resourceName: "unLiked"), for: .normal)
        cell.likeButton.tag = indexPath.row
        cell.likeButton.addTarget(self, action: #selector(likeBtnTapped), for: .touchUpInside)
        
        cell.likeCountLbl.text = posts[indexPath.row].likeCount! == 1 ? "\(posts[indexPath.row].likeCount!) like" : "\(posts[indexPath.row].likeCount!) likes"
        
        return cell
    }
    
    func likeBtnTapped(_ sender: UIButton) {
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = self.tblView.cellForRow(at: indexPath) as! PostViewCell
        
        if posts[sender.tag].isLikedByUser {
            posts[sender.tag].isLikedByUser = false
            sender.setImage(#imageLiteral(resourceName: "unLiked"), for: .normal)
            posts[sender.tag].likeCount! -=  1
        } else {
            posts[sender.tag].isLikedByUser = true
            sender.setImage(#imageLiteral(resourceName: "liked"), for: .normal)
            posts[sender.tag].likeCount! +=  1
        }
        
        Model().updateLikeStatus(uID: uID!, post: posts[sender.tag])
        cell.likeCountLbl.text = posts[indexPath.row].likeCount! == 1 ? "\(posts[indexPath.row].likeCount!) like" : "\(posts[indexPath.row].likeCount!) likes"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

