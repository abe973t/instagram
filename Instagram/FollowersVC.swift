//
//  FollowersVC.swift
//  Instagram
//
//  Created by Abraham Tesfamariam on 7/20/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

import UIKit
import FirebaseAuth
import TWMessageBarManager

class FollowersVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    // add a search bar, new design from screenshot, selectCell to add followers in collView
    
    @IBOutlet weak var collView: UICollectionView!
    var users: [[String:Any]] = []
    var followers: [String] = []
    var indexForCellTapped: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Users"
        
        // get users
        // have the users function return followers as well
        Model().getUsers { (userArr) in
            if let userArray = userArr as? [[String:Any]] {
                self.users = userArray
            }
            DispatchQueue.main.async {
                self.collView.reloadData()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UsersCollectionViewCell", for: indexPath) as! UsersCollectionViewCell
        
        // setup cell image
        cell.userImgView.layer.cornerRadius = cell.userImgView.frame.width / 2
        cell.userImgView.clipsToBounds = true
        cell.userImgView.layer.borderWidth = 1.5
        cell.userImgView.layer.borderColor = UIColor.black.cgColor
        
        let user = users[indexPath.row]
        print(user["userName"] as! String)
        print(user["imgURL"] as! String)
        cell.userImgView.sd_setImage(with: URL(string: user["imgURL"] as! String)!)
        cell.userNameLabel.text = user["userName"] as? String
        followers.contains(user["UID"] as! String) ? cell.followBtn.setImage(#imageLiteral(resourceName: "followedIcon"), for: .normal) : cell.followBtn.setImage(#imageLiteral(resourceName: "followIcon"), for: .normal)
        
        cell.messageBtn.tag = indexPath.row
        cell.followBtn.tag = indexPath.row
        cell.messageBtn.addTarget(self, action: #selector(messageBtnTapped), for: .touchUpInside)
        cell.followBtn.addTarget(self, action: #selector(followBtnTapped), for: .touchUpInside)
        
        return cell
    }
    
    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func messageBtnTapped(_ sender: UIButton) {
        
        if let dVC = storyboard?.instantiateViewController(withIdentifier: "MessagesVC") as? MessagesVC {
            let user = users[sender.tag]
            
            //dVC.user = user
            present(dVC, animated: true, completion: nil)
        }
    
    }
    
    func followBtnTapped(_ sender: UIButton) {
        let user = users[sender.tag]

        if followers.contains(user["UID"] as! String) {
            TWMessageBarManager().showMessage(withTitle: "Notification", description: "You have unfollowed: \(user["userName"]!)", type: .error)
        } else {
            followers.append(user["UID"]! as! String)
            TWMessageBarManager().showMessage(withTitle: "Notification", description: "You are now following: \(user["userName"]!)", type: .success)
        }
        
        Model().updateFollowers(user: (Auth.auth().currentUser?.uid)!, follower: user["UID"] as! String)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        indexForCellTapped = indexPath.row
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UsersCollectionViewCell", for: indexPath) as? UsersCollectionViewCell
        
        cell?.userNameLabel.isHidden = true
        cell?.followBtn.isHidden = false
        cell?.messageBtn.isHidden = false
        
        
    collectionView.cellForItem(at: indexPath)?.backgroundColor = UIColor.init(red: 60, green: 60, blue: 60, alpha: 0.7)
        
        print(followers)
    }
    
    func messageBtnClicked() {
        if let dVC = storyboard?.instantiateViewController(withIdentifier: "MessagesVC") as? MessagesVC {
            //dVC.user = user["userName"] as? String
            
            self.navigationController?.pushViewController(dVC, animated: true)
        }
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
