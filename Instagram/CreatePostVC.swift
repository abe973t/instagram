//
//  CreatePostVC.swift
//  Instagram
//
//  Created by Abraham Tesfamariam on 7/21/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

import UIKit
import FirebaseAuth
import FacebookCore
import FBSDKShareKit
import SCLAlertView

// PROBLEM: posting new photo overwrites all other posts by the user, 

class CreatePostVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate {
    
    @IBOutlet weak var tkePhotoBtn: UIButton!
    @IBOutlet weak var newPostImg: UIImageView!
    @IBOutlet weak var descTxtView: UITextView!
    var imgPicker = UIImagePickerController()
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var addPhotoBtn: UIButton!
    var placeholder = "Caption your photo here!! Please limit to 140 characters."
    let content = FBSDKSharePhotoContent()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Create Post"
        
        shareBtn.isEnabled = false
        descTxtView.text = placeholder
        
        if newPostImg.image != nil {
            shareBtn.isEnabled = true
            addPhotoBtn.isHidden = true
            tkePhotoBtn.isHidden = true
        }
        
        let fbShareBtn = FBSDKShareButton()
        fbShareBtn.shareContent = content
        fbShareBtn.frame = CGRect(x: 20, y: 600, width: 100, height: 40)
        self.view.addSubview(fbShareBtn)
    }
    
    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        if shareBtn.isEnabled {
            newPostImg.image = nil
            addPhotoBtn.isHidden = false
            tkePhotoBtn.isHidden = false
            shareBtn.isEnabled = false
        } else {
            dismiss(animated: true, completion: nil)
        }
    }

    // replaces placeholder with user input
    func textViewDidBeginEditing(_ textView: UITextView) {
        if descTxtView.text == placeholder {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    // fills textView with placeholder if user deletes the text
    func textViewDidEndEditing(_ textView: UITextView) {
        if descTxtView.text.isEmpty {
            textView.textColor = UIColor.lightGray
            descTxtView.text = placeholder
        }
    }

    @IBAction func shareBtnPressed(_ sender: UIButton) {
        if newPostImg.image == nil {
            let alrt = UIAlertController(title: "Alert", message: "Need a photo to share.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alrt.addAction(ok)
            present(alrt, animated: true, completion: nil)
        } else {
            Model().postPhoto(UID: (Auth.auth().currentUser?.uid)!, photo: newPostImg.image!, desc: descTxtView.text)
            SCLAlertView().showSuccess("Success!", subTitle: "Your post was shared.", closeButtonTitle: nil, duration: 3, colorStyle: 0xff9100, colorTextButton: 0x3e2723, circleIconImage: nil, animationStyle: .bottomToTop)
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func openGalleryAction(_ sender: UIButton) {
        imgPicker.sourceType = .photoLibrary
        imgPicker.allowsEditing = true
        imgPicker.delegate = self
        present(imgPicker, animated: true, completion: nil)
        
        if newPostImg.image != nil {
            shareBtn.isEnabled = true
            addPhotoBtn.isHidden = true
            tkePhotoBtn.isHidden = true
        }
    }
    
    @IBAction func openCamera(_ sender: UIButton) {
        // checks if camera is available first, presents alert if not available
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imgPicker.sourceType = .camera
            imgPicker.delegate = self
            present(imgPicker, animated: true, completion: nil)
        } else {
            let alrt = UIAlertController(title: "Caution", message: "Camera not available.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alrt.addAction(ok)
            present(alrt, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        newPostImg.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        dismiss(animated: true) { 
            if self.newPostImg.image != nil {
                self.addPhotoBtn.isHidden = true
                self.tkePhotoBtn.isHidden = true
                self.shareBtn.isEnabled = true
                let photo = FBSDKSharePhoto(image: self.newPostImg.image, userGenerated: true)
                self.content.photos = [photo!]
                FBSDKShareDialog.show(from: self, with: self.content, delegate: nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
