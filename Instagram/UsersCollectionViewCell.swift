//
//  UsersCollectionViewCell.swift
//  Instagram
//
//  Created by Abraham Tesfamariam on 7/20/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

import UIKit

class UsersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userImgView: UIImageView!
    
    @IBOutlet weak var followBtn: UIButton!
    
    @IBOutlet weak var messageBtn: UIButton!
    

}
