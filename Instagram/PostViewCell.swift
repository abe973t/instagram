//
//  PostViewCell.swift
//  Instagram
//
//  Created by Abraham Tesfamariam on 7/22/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

import UIKit

class PostViewCell: UITableViewCell {

    @IBOutlet weak var profilePicImgView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var postImgView: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var captionTxtView: UITextView!
    @IBOutlet weak var likeCountLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        captionTxtView.isEditable = false
        
        // make imgView circular
        profilePicImgView.layer.cornerRadius = profilePicImgView.frame.width / 2
        profilePicImgView.clipsToBounds = true
        // add border
        profilePicImgView.layer.borderWidth = 1.5
        profilePicImgView.layer.borderColor = UIColor.orange.cgColor
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
