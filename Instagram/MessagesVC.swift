//
//  MessagesVC.swift
//  Instagram
//
//  Created by Abraham Tesfamariam on 7/23/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

import UIKit
import Firebase
import JSQMessagesViewController

enum Section: Int {
    case createNewChannelSection = 0
    case currentChannelsSection
}

class MessagesVC: JSQMessagesViewController {
    
    //private lazy var channelRef: FIRDatabaseReference = FIRDatabase.database().reference().child("channels")
    //private var channelRefHandle: FIRDatabaseHandle?

    var messages: [JSQMessage] = []
    let user: [String:Any]? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Messages"
        
        //MessagesHandler.Instance.delegate = self
        
        self.senderId = user!["UID"] as! String
        self.senderDisplayName = user!["userName"] as! String
        
        //MessagesHandler.Instance.observeMessages()
        //MessagesHandler.Instance.observeMediaMessages()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
