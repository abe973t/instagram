//
//  LoginVC.swift
//  Instagram
//
//  Created by Abraham Tesfamariam on 7/19/17.
//  Copyright © 2017 RJT Compuquest. All rights reserved.
//

import UIKit
import FirebaseAuth
import FacebookLogin
import FBSDKLoginKit
import SCLAlertView

// NEED TO IMPLEMENT FORGOT PASSWORD BTN

class LoginVC: UIViewController, FBSDKLoginButtonDelegate {
    
    @IBOutlet weak var userNameTxtField: UITextField!
    @IBOutlet weak var pwdTxtField: UITextField!
    @IBOutlet var loginView: UIView!
    @IBOutlet var signUpView: UIView!
    
    @IBOutlet weak var newUserTxtField: UITextField!
    @IBOutlet weak var reenterPassTxtField: UITextField!
    @IBOutlet weak var newPassTxtField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // read permissions for fbLogin
        let fbLoginBtn = FBSDKLoginButton()
        loginView.addSubview(fbLoginBtn)
        fbLoginBtn.delegate = self
        fbLoginBtn.frame = CGRect(x: 100, y: 500, width: 200, height: 50)

    }
    
    // login with fb
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error?) {
        
        let alrt = UIAlertController(title: "Error", message: "Login authentication failed!", preferredStyle: .alert)
        let ok = UIAlertAction(title: "ok", style: .default, handler: nil)
        alrt.addAction(ok)
        
        if let error = error {
            print(error.localizedDescription)
            return
        }
        
        // get an access token for the signed-in user and exchange it for a Firebase credential
        let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
        
        // authenticate with Firebase using the Firebase credentia
        Auth.auth().signIn(with: credential) { (user, error) in
            if error != nil {
                self.present(alrt, animated: true, completion: nil)
            } else {
                DispatchQueue.main.async {
                    let dVC = self.storyboard?.instantiateViewController(withIdentifier: "navController")
                    self.present(dVC!, animated: true, completion: nil)
                }
            }
        }
    
    }

    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("Hey you logged out.")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginAction(_ sender: Any) {
        let alrt = UIAlertController(title: "Error", message: "Login authentication failed!", preferredStyle: .alert)
        let ok = UIAlertAction(title: "ok", style: .default, handler: nil)
        alrt.addAction(ok)
        
        Auth.auth().signIn(withEmail: userNameTxtField.text!, password: pwdTxtField.text!) { (user, error) in
            if(error != nil) {
                self.present(alrt, animated: true, completion: nil)
            } else {
                DispatchQueue.main.async {
                    let dVC = self.storyboard?.instantiateViewController(withIdentifier: "navController")
                    self.present(dVC!, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func willSignUp () {
        self.view = signUpView
    }
    
    @IBAction func signUpAction () {
       
        if(newPassTxtField.text != reenterPassTxtField.text) {
            SCLAlertView().showError("Error!", subTitle: "Passwords do not match.", closeButtonTitle: nil, duration: 3, colorStyle: 0xd50000, colorTextButton: 0x212121, circleIconImage: nil, animationStyle: .bottomToTop)

        } else {
            Auth.auth().createUser(withEmail: newUserTxtField.text!, password: reenterPassTxtField.text!) { (user, error) in
                print("Error: ", error ?? "nil")
                print("User: ", user ?? "nil")
            }
            Auth.auth().currentUser?.sendEmailVerification(completion: nil)
            SCLAlertView().showSuccess("Success!", subTitle: "Your post was shared.", closeButtonTitle: nil, duration: 3, colorStyle: 0xff9100, colorTextButton: 0x3e2723, circleIconImage: nil, animationStyle: .bottomToTop)
            
            if let dVC = storyboard?.instantiateViewController(withIdentifier: "HomeVC") as? HomeVC {
                present(dVC, animated: true, completion: nil)
            }
        }
        
    }
    
    @IBAction func cancelSignUpAction(_ sender: UIButton) {
        self.view = loginView
    }

}
